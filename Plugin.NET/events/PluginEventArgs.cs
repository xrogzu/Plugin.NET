﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginNET.events
{
    /// <summary>
    /// 事件参数的基类
    /// </summary>
    [Serializable]
    public abstract class PluginEventArgs : EventArgs
    {
        /// <summary>
        /// 取消继续执行操作，设置为true为阻止本次事件后面的代码继续执行
        /// </summary>
        public bool Cancel { get; set; }
    }
}
