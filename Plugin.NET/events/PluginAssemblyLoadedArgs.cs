﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PluginNET.events
{
    /// <summary>
    /// Assembly加载前事件的参数
    /// </summary>
    public class PluginAssemblyLoadedArgs : PluginAssemblyLoadingArgs
    {
        /// <summary>
        /// 加载的程序集对象
        /// </summary>
        public Assembly Assembly { get; internal set; }
    }
}
